[1.7] 2016-09-06
- Added new icons - walls, treeguards, shipwrecked bosses

[1.6] 2016-06-26
- Added new icons - water beefalo, wildbore heads

[1.5] 2016-06-23
- Added new icons - crabbit dens, packim fishbone

[1.4.1] 2016-06-17
- Categorised options in configuration page
- Fixed some typos

[1.4] 2016-06-17
- Added new icons - doydoy, baby doydoy
- Reduced size of baby beefalo icon

[1.3] 2016-06-16
- Added new icons - pig heads, merm heads, animal tracks

[1.2] 2016-06-15
- Added new icons - ballphin, swordfish
- Updated mod icon

[1.1] 2016-06-14
- Added new icons - clockwork creatures, bones, teleportato things, pig torches

[1.0] 2016-06-13
- Added the same icons as Where's My Beefalo but with newly assembled assets
- Added new icons - flotsam, skeleton, tentacle
- Added mod icon

[0.1] 2016-06-12
- Added basic functionality
- Added flotsam as a map icon
- Added commented entities for future icon assets
