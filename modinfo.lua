
name = "Extended Map Icons"
version = "1.7"
description = "Add a greater variety of icons to your map!\n\nVersion 1.7"
author = "leo"

forumthread = ""

api_version = 6

dont_starve_compatible = true
reign_of_giants_compatible = true
shipwrecked_compatible = true

icon = "modicon.tex"
icon_atlas = "modicon.xml"

configuration_options = {
  {
    name = "MOBS",
    label = "MOBS",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "babybeefalo",
    label = "Baby Beefalo",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "beefalo",
    label = "Beefalo",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "bishop",
    label = "Clockwork Bishop",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "knight",
    label = "Clockwork Knight",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "rook",
    label = "Clockwork Rooks",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "leif_sparse",
    label = "Lumpy Treeguards",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "rocky",
    label = "Rock Lobsters",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "tentacle",
    label = "Tentacles",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "leif",
    label = "Treeguards",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "lightninggoat",
    label = "Volt Goats",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "MOBS - KOALEFANTS",
    label = "MOBS - KOALEFANTS",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "koalefant_summer",
    label = "Summer Koalefants",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "koalefant_winter",
    label = "Winter Koalefants",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "MOBS - GIANTS",
    label = "MOBS - GIANTS",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "bearger",
    label = "Bearger",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "deerclops",
    label = "Deerclops",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "dragonfly",
    label = "Dragonfly",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "moose",
    label = "Moose/Goose",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "MOBS - SHIPWRECKED",
    label = "MOBS - SHIPWRECKED",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "doydoybaby",
    label = "Baby Doydoys",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "ballphin",
    label = "Bottlenosed Ballphins",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "doydoy",
    label = "Doydoys",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "swordfish",
    label = "Swordfish",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "ox",
    label = "Water Beefalo",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "MOBS - SHIPWRECKED BOSSES",
    label = "MOBS - SHIPWRECKED BOSSES",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "treeguard",
    label = "Palm Treeguards",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "kraken",
    label = "Quacken",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "twister",
    label = "Sealnado",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "tigershark",
    label = "Tiger Shark",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "VEGETATION",
    label = "VEGETATION",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "carrot_planted",
    label = "Carrots",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "mandrake",
    label = "Mandrakes",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "MUSHROOMS",
    label = "MUSHROOMS",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "blue_mushroom",
    label = "Blue Mushrooms",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "green_mushroom",
    label = "Green Mushrooms",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "red_mushroom",
    label = "Red Mushrooms",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "STRUCTURES",
    label = "STRUCTURES",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "houndbone",
    label = "Bones",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "mermhead",
    label = "Merm Heads",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "pighead",
    label = "Pig Heads",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "pigtorch",
    label = "Pig Torches",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "skeleton",
    label = "Skeletons",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "wildborehead",
    label = "Wildbore Heads",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "STRUCTURES - WALLS",
    label = "STRUCTURES - WALLS",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "wall_hay",
    label = "Hay Walls",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "wall_limestone",
    label = "Limestone Walls",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "sandbagsmall",
    label = "Sandbags",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "wall_stone",
    label = "Stone Walls",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "wall_ruins",
    label = "Thulecite Walls",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "wall_wood",
    label = "Wood Walls",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "ITEMS",
    label = "ITEMS",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "chester_eyebone",
    label = "Chester's Eye Bone",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "flint",
    label = "Flint",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "packim_fishbone",
    label = "Packim's Fishbone",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "THINGS",
    label = "THINGS",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "teleportato_box",
    label = "Box Thing",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "teleportato_crank",
    label = "Crank Thing",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "teleportato_potato",
    label = "Metal Potato Thing",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "teleportato_ring",
    label = "Ring Thing",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }

  },  {
    name = "MISCELLANEOUS",
    label = "MISCELLANEOUS",
    default = "true",
    options = { {description = "", data = "true"} }
  },  {
    name = "animal_track",
    label = "Animal Tracks",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "crabhole",
    label = "Crabbit Dens",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "flotsam",
    label = "Flotsams",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "molehill",
    label = "Molehills",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }
  },  {
    name = "rabbithole",
    label = "Rabbit Holes",
    default = "true",
    options = { {description = "True", data = "true"}, {description = "False", data = "false"} }


  }
}
